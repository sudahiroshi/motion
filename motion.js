/*window.ondevicemotion = function(event) {
    document.getElementById("accelerationX").innerHTML = event.acceleration.x;
    document.getElementById("accelerationY").innerHTML = event.acceleration.y;
    document.getElementById("accelerationZ").innerHTML = event.acceleration.z;
    document.getElementById("accelerationIncludingGravityX").innerHTML = event.accelerationIncludingGravity.x;
    document.getElementById("accelerationIncludingGravityY").innerHTML = event.accelerationIncludingGravity.y;
    document.getElementById("accelerationIncludingGravityZ").innerHTML = event.accelerationIncludingGravity.z;
}*/
console.log("task0");
window.addEventListener('load', () => {
    document.querySelector("#test").innerHTML = "task1";
    console.log("task1");
    var ax = 0;
    var x = 0;
    var acxa = new Array(960);
    var axa = new Array(960);
    var xa = new Array(960);

    for( let i=0; i<960; i++ ) {
        acxa[i] = 0;
        axa[i] = 0;
        xa[i] = 0;
    }

    var canvas1 = document.querySelector( "#view" );
    var ctx1 = canvas1.getContext('2d');
    window.addEventListener('devicemotion', (ev) => {
        //document.querySelector("#test").innerHTML = "foo";
        console.log("task2");
        let acx = Math.trunc( ev.acceleration.x * 10 ) / 10.0;
        //let acx = ev.acceleration.x;
        ax = ax * .9 + acx;
        x += ax;
        for( let i=959; i>0; i-- ) {
            acxa[i] = acxa[i-1];
            axa[i] = axa[i-1];
            xa[i] = xa[i-1];
        }
        //document.querySelector("#test").innerHTML = "bar";
        acxa[0] = acx * 5.0;
        ctx1.beginPath();
        ctx1.clearRect( 0, 0, 480, 960 );
        ctx1.strokeStyle = "rgb( 250, 200, 160)";
        ctx1.moveTo( 240, 0 );
        for( let i=0; i<960; i++ ) {
            ctx1.lineTo( 240 + acxa[i], i );
        }
        ctx1.stroke();

        axa[0] = ax * 5.0;
        ctx1.beginPath();
        ctx1.strokeStyle = "rgb( 160, 200, 250)";
        ctx1.moveTo( 240, 0 );
        for( let i=0; i<960; i++ ) {
            ctx1.lineTo( 240 + axa[i], i );
        }
        ctx1.stroke();

        xa[0] = x / 2.0;
        ctx1.beginPath();
        ctx1.strokeStyle = "rgb( 160, 250, 200)";
        ctx1.moveTo( 240, 0 );
        for( let i=0; i<960; i++ ) {
            ctx1.lineTo( 240 + xa[i], i );
        }
        ctx1.stroke();

        //document.querySelector("#test").innerHTML = "baz";
        document.querySelector("#accX").innerHTML = acx;
        document.querySelector("#ax").innerHTML = ax;
        document.querySelector("#x").innerHTML = x;
    });
    document.querySelector("#reset").addEventListener('click',()=>{
        ax = 0;
        x = 0;
    })
});